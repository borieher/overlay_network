#!/bin/bash

# NOTES:
# requires bash 4.0 or superior to work

# specify the following variables in set_environment() function:
# CUSTOM_IMAGE_NAME
# RESPOSITORY_NAME
# OVERLAY_SUBNET
# OVERLAY_MASK

# global variables
declare -a container_names
RUN_ON_VM="1"

function usage(){
    cat <<EOF
    Usage:

    Use --setup-dockerfile or --setup-download to get the Docker base image.
    Please use only one of these options:
        --setup-dockerfile expects to run the script at the same location as the Dockerfile.
        --setup-download retrieves the image from the Docker Hub, so it needs Internet connection.

    Use -c or --container-names and pass either a CSV formatted file or a CSV formatted string.
    For a CSV file: -c example.txt
    For a CSV string: -c some,random,values
    The CSV parser checks for spaces and newlines, but not for strange characters, so keep it in mind.

    Use -d or --deploy and pass bridge_ip/bridge_mask and host_ip/host_mask values.
    bridge_ip/bridge_mask is used to create the overlay network. It has the format 192.168.15.1/24.
    Currently the scripts only uses 192.168.15.0/24 network.
    host_ip/host_mask is the IP address and mask of the computer running the containers.

    Use -4 or --step-four to deploy the necessary changes to connect two overlay networks in different hosts
    in the same LAN. 
    Please use it after running the deploy command.
    number_of_containers is used to specify the number of containers running in the host to connect with.
    external_bridge_ip/mask is used to create the routes using it as starting IP address.
    external_host_ip/mask is used to route the traffic to that host.

    Use -t or --tear-down and pass the CSV with the container names, bridge_ip/bridge_mask and host_ip/host_mask values.
    This option needs to be run alone. The values are the same mentioned above.

    Currently the script creates this:
                                    br-overlay (192.168.15.1/24)
                                   /     |      \\
                             name1     name2     name3
                (192.168.15.2/24)  (192.168.15.3/24)  (192.168.15.4/24)
    
    This will be the output of running the following command:
    ./overlay_network --setup-download -c name1,name2,name3 -d 192.168.15.1/24 X.X.X.X/X

    To connect with other host's subnet:
    ./overlay_network -4 2 192.168.15.5/24 X.X.X.X/X
    This example assummes that the host X.X.X.X has 2 containers and the bridge of the subnet has
    the address 192.168.15.4

    To tear down the example:
    ./overlay_network -t name1,name2,name3 192.168.15.1/24 X.X.X.X/X
EOF
}

function print_deployment_info(){
    for index in ${!container_names[@]}; do
        echo "Container name: ${container_names[index]}, Container id: $(docker inspect --format '{{ .ID }}' ${container_names[$index]}), IP address: ${overlay_subnet}.$((${br_octet}+${index}+1))/${overlay_mask} and port: $((5000+${br_octet}+${index}+1))"
    done
}

function set_environment(){
    # error when variables are not set
    set -u
    # stop when errors occur
    set -e

    # configuration constants
    REPOSITORY_NAME="borieher"
    CUSTOM_IMAGE_NAME="overlay_node"
    OVERLAY_SUBNET="192.168.15"
    OVERLAY_MASK="24"

    if [[ "${RUN_ON_VM}" == "1" ]]; then
        # if running on VM deactivate iptables on bridge
        sudo sysctl net.bridge.bridge-nf-call-iptables=0 &> /dev/null
    fi

    # enable FORWARD on iptables to remove docker FORWARD DROP rule
    set +e
        sudo iptables -P FORWARD ACCEPT &> /dev/null
    set -e
}

# this function receives a SUBNET/MASK (192.168.15.0/24) string and parses it
# in SUBNET_RANGE (192.168.1.0) returning the SUBNET on success, 1 on failure
function validate_subnet(){
    # grab the four subnet octets by defining record and field separators. Only output first record.
    SUBNET=$(<<< "${1}" awk 'BEGIN{ RS="/"; FS="."; }NR==1{print $1"."$2"."$3"."$4}')

    # check subnet_range format match (X.X.X.X)
    SUBNET_FORMAT_RE='^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$'
    if ! [[ "${SUBNET}" =~ ${SUBNET_FORMAT_RE} ]]; then
        echo "Invalid subnet: ${SUBNET}"
        return 1
    fi

    echo "${SUBNET}"
}

# this function receives a SUBNET/MASK (192.168.15.0/24) string and parses it
# in MASK (8, 16 or 24) returning the MASK on success, 1 on failure
function validate_mask(){
    # grab the mask
    MASK=$(<<< "${1}" awk -F '/' '{print $2}')

    if ! [[ "${MASK}" =~ ^8|16|24$ ]]; then
        echo "Invalid mask: ${MASK}. Only 8, 16 or 24 accepted"
        return 1
    fi

    echo "${MASK}"
}

# specific function to work with 192.168.15.0/24 network
# and set up this example by using the base ip in (X.X.X) format
function validate_base_ip(){
    # TODO: use ${OVERLAY_SUBNET} in the pattern below
    if ! [[ "${1}" =~ '^192.168.15\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$' || "${2}" =~ ^24$ ]]; then
        echo "Bridge IP address must be on ${OVERLAY_SUBNET}.0/${OVERLAY_MASK} network"
        return 1
    fi

    base_ip=$(<<< "${1}" awk -F '.' '{print $1"."$2"."$3}')

    echo "${base_ip}"
}

# creation of the image based on the Dockerfile
# needs to be executed in the same path as the Dockerfile
function create_image_from_dockerfile(){
    echo "Create docker base image named: ${REPOSITORY_NAME}/${CUSTOM_IMAGE_NAME} from Dockerfile"

    # check if base image is downloaded, if not download it
    if [[ -z $(docker images -q ubuntu:latest) ]]; then
        docker image pull ubuntu:latest
    fi

    # check the image is not built/downloaded
    # TODO: repair bug
    if [[ ! -z $(docker images -q "${REPOSITORY_NAME}"/"${CUSTOM_IMAGE_NAME}") ]]; then
        # create custom docker image called overlay_node from Dockerfile
        docker build -t "${REPOSITORY_NAME}"/"${CUSTOM_IMAGE_NAME}" .
        echo "Docker image: ${REPOSITORY_NAME}/${CUSTOM_IMAGE_NAME} created from Dockerfile"
    else
        echo "Skipping image creation from Dockerfile, image present"
    fi

    if [[ ! -z $(docker images -q "${REPOSITORY_NAME}"/"${CUSTOM_IMAGE_NAME}") ]]; then
        # create custom docker image called overlay_node from Dockerfile
        echo "Docker image: ${REPOSITORY_NAME}/${CUSTOM_IMAGE_NAME} created from Dockerfile"
    else
        echo "Skipping image creation from Dockerfile, image present"
    fi
}

# TODO: why it only works if I execute this first?
# download the docker image from Docker Hub
function download_docker_image(){
    echo "Download docker base image named: ${REPOSITORY_NAME}/${CUSTOM_IMAGE_NAME} from Docker Hub"

    # check if image is downloaded, if not download it
    if [[ -z $(docker images -q "${REPOSITORY_NAME}"/"${CUSTOM_IMAGE_NAME}") ]]; then
        docker image pull "${REPOSITORY_NAME}"/"${CUSTOM_IMAGE_NAME}":latest
        echo "Docker image: ${REPOSITORY_NAME}/${CUSTOM_IMAGE_NAME} downloaded from Docker Hub"
    else
        echo "Skipping image download from Docker Hub, image present"
    fi
}

function get_container_names(){
    names_csv="${1}"

    if [[ -f "$names_csv" ]]; then
        # file parsing code
        echo "User specified the container names in the file: ${names_csv}"
        # remove new lines with tr and parse CSV with awk removing whitespaces and blank fields
        container_names=($(awk 'BEGIN{ FS=","; }{ for(i=1; i<=NF; i++){ gsub(" ",""); if($i!=""){ print $i }}}' <(tr -d '\n' < $names_csv)))
    else
        # grab the CSV from the string with awk
        echo "User specified the container names as a bash string"
        # parse CSV with awk removing whitespaces and blank fields
        container_names=($(echo "$names_csv" | awk 'BEGIN{ FS=","; }{ for(i=1; i<=NF; i++){ print $i }}'))
    fi

}

function create_containers(){
    echo "Creating the containers with the names specified"
    for index in ${!container_names[@]}; do
        # --cap-add=NET_ADMIN to use iproute2
        docker container run -d --name "${container_names[$index]}" --cap-add=NET_ADMIN --network=none "${REPOSITORY_NAME}"/"${CUSTOM_IMAGE_NAME}" &> /dev/null
    done
}

function create_overlay_network(){
    bridge_ip=$(validate_subnet ${1})

    # store bridge last octet from ip address to start giving the containers adresses based on that
    br_octet=$(<<< "${bridge_ip}" awk -F '.' '{print $4}')

    overlay_mask=$(validate_mask ${1})

    overlay_subnet=$(validate_base_ip ${bridge_ip} ${overlay_mask})

    echo "Setting up the overlay network. By default it uses the ${OVERLAY_SUBNET}.0/${OVERLAY_MASK} network"
    echo "Using ${bridge_ip} as starting IP address and ${overlay_subnet}.X as base"

    # Creation of the bridge
    sudo ip link add br-overlay type bridge
    sudo ip addr add "${bridge_ip}"/"${overlay_mask}" dev br-overlay
    sudo ip link set br-overlay up

    # Creation of the veth pairs. To connect the containers to the bridge
    for index in ${!container_names[@]}; do
        sudo ip link add veth-"${container_names[$index]}" type veth peer name veth-"${container_names[$index]}"-br
        # add veth pair to container and configure it
        # docker inspect --format '{{ .State.Pid }}' XXXX is the container ID, to get the PID of the container
        sudo ip link set veth-"${container_names[$index]}" netns $(docker inspect --format '{{ .State.Pid }}' ${container_names[$index]})
        # we add to the bridge address the index of the container plus 1 (index starts at 0) and use it as container ip address
        docker container exec "${container_names[$index]}" ip addr add "${overlay_subnet}".$((${br_octet}+${index}+1))/"${overlay_mask}" dev veth-"${container_names[$index]}"
        docker container exec "${container_names[$index]}" ip link set veth-"${container_names[$index]}" up

        sudo ip link set veth-"${container_names[$index]}"-br master br-overlay
        sudo ip link set veth-"${container_names[$index]}"-br up
    done

    add_LAN_and_default_route "${2}"

    expose_NAT_services

    print_deployment_info
}

# to reach LAN from inside the containers
# expects to run inside create_overlay_network where some variables used exist
# receives HOST_IP/HOST_MASK as parameter
function add_LAN_and_default_route(){
    host_ip=$(validate_subnet ${1})
    host_mask=$(validate_mask ${1})

    host_subnet=$(<<< "${host_ip}" awk -F '.' '{print $1"."$2"."$3".0"}')

    echo "Adding route to LAN: ${host_ip}/${host_mask} and default route via: ${bridge_ip}"

    for index in ${!container_names[@]}; do
        docker container exec "${container_names[$index]}" ip route add "${host_subnet}"/"${host_mask}" via "${bridge_ip}"
        docker container exec "${container_names[$index]}" ip route add default via "${bridge_ip}"
    done

    # adds masquerading to the packets leaving the host, in order to surpass NAT and come back
    # deactivate -e to continue even though errors appear
    # use iptables -C option to see if rule exists (returns error code 1, that's why -e is deactivated)
    set +e
        sudo iptables -t nat -C POSTROUTING -s "${overlay_subnet}".0/"${overlay_mask}" -j MASQUERADE &> /dev/null
        if [[ $? != 0 ]]; then
            sudo iptables -t nat -A POSTROUTING -s "${overlay_subnet}".0/"${overlay_mask}" -j MASQUERADE &> /dev/null
        fi
    set -e

    # enable ipv4 forwarding
    sudo sysctl net.ipv4.ip_forward=1 &> /dev/null
}

# function to expose the container's webserver to other members of the host LAN
function expose_NAT_services(){
    echo "Exposing Web servers through NAT"
    set +e
        for index in ${!container_names[@]}; do
            # expose TCP port 5000+br_octet+index+1 for the Web server running in the container at host_ip if iptables rule doesn't exist
            sudo iptables -t nat -C PREROUTING -p tcp -d "${host_ip}" --dport $((5000+${br_octet}+${index}+1)) -j DNAT --to-destination "${overlay_subnet}".$((${br_octet}+${index}+1)):5000 &> /dev/null
            if [[ $? != 0 ]]; then
                sudo iptables -t nat -A PREROUTING -p tcp -d "${host_ip}" --dport $((5000+${br_octet}+${index}+1)) -j DNAT --to-destination "${overlay_subnet}".$((${br_octet}+${index}+1)):5000 &> /dev/null
            fi
        done
    set -e
}

# function to pass step four
function step_four(){
    echo "Adding step four routes to the example"
    numof_ext_containers="${1}" # number of external containers

    # "${2}" has the external overlay subnet/mask ip address
    ext_bridge_ip=$(validate_subnet ${2})
    ext_mask=$(validate_mask ${2})

    ext_br_octet=$(<<< "${ext_bridge_ip}" awk -F '.' '{print $4}')

    ext_base_subnet=$(validate_base_ip ${ext_bridge_ip} ${ext_mask})

    ext_host_ip=$(validate_subnet ${3}) # external host ip addres/mask

    for ((i=0; i<=$numof_ext_containers; i++)); do
        sudo ip route add "${ext_base_subnet}".$((${ext_br_octet}+${i})) via "${ext_host_ip}"
    done
}

# function to tear down all elements
function tear_down(){
    echo "Tearing down deployment, keep calm it takes a while"

    get_container_names "${1}"
    bridge_ip=$(validate_subnet ${2})
    # store bridge last octet from ip address to start giving the containers adresses based on that
    br_octet=$(<<< "${bridge_ip}" awk -F '.' '{print $4}')

    overlay_mask=$(validate_mask ${2})
    overlay_subnet=$(validate_base_ip ${bridge_ip} ${overlay_mask})

    host_ip=$(validate_subnet ${3})
    host_mask=$(validate_mask ${3})

    if [[ "${RUN_ON_VM}" == "1" ]]; then
        # if running on VM activate iptables on bridge when tearing down
        sudo sysctl net.bridge.bridge-nf-call-iptables=1 &> /dev/null
    fi

    set +e
        # leave iptables as docker sets it
        sudo iptables -P FORWARD DROP &> /dev/null

        sudo iptables -t nat -C POSTROUTING -s "${overlay_subnet}".0/"${overlay_mask}" -j MASQUERADE &> /dev/null
        if [[ $? == 0 ]]; then
            sudo iptables -t nat -D POSTROUTING -s "${overlay_subnet}".0/"${overlay_mask}" -j MASQUERADE &> /dev/null
        fi

        for index in ${!container_names[@]}; do
            sudo iptables -t nat -C PREROUTING -p tcp -d "${host_ip}" --dport $((5000+${br_octet}+${index}+1)) -j DNAT --to-destination "${overlay_subnet}".$((${br_octet}+${index}+1)):5000 &> /dev/null
            if [[ $? == 0 ]]; then
                sudo iptables -t nat -D PREROUTING -p tcp -d "${host_ip}" --dport $((5000+${br_octet}+${index}+1)) -j DNAT --to-destination "${overlay_subnet}".$((${br_octet}+${index}+1)):5000 &> /dev/null
            fi
            docker container stop "${container_names[$index]}" > /dev/null
            docker container rm "${container_names[$index]}" > /dev/null
        done
    set -e

    sudo ip link set br-overlay down
    sudo ip link delete br-overlay
    echo "Deployment destroyed"
}

function parse_arguments(){

    ARGS_NUMBER="$#" # save the number of arguments provided

    while [[ "$#" -gt 0 ]]; do

        key="${1}" # every iteration gets changed by the last shift command

        case "${key}" in
        --setup-dockerfile)
            create_image_from_dockerfile
        ;;

        --setup-download)
            download_docker_image
        ;;

        -c | --container_names)
            get_container_names "${2}"
            shift # skip value
        ;;

        -d | --deploy)
            create_containers
            create_overlay_network "${2}" "${3}"
            shift # skip value
            shift # skip value
        ;;

        -4 | --step-four)
            step_four "${2}" "${3}" "${4}"
            shift # skip value
            shift # skip value
            shift # skip value
        ;;

        -t | --tear-down)
            tear_down "${2}" "${3}" "${4}"
            shift # skip value
            shift # skip value
            shift # skip value
            exit 0
        ;;

        -h | --help) # help has priority over every command
            usage
            exit 0
        ;;
        
        *)
            echo "Unrecognized option ${1}" # default, matches everything not specified
            exit 1
        ;;
        esac

        shift # next key
    done
}

function main(){
    set_environment
    parse_arguments "$@" # pass all arguments to function
}

main "$@" # pass all arguments to function