# Explicación del script para la creación de la red overlay

## Ejemplo de ejecución para la obtención del resultado de los puntos 1 a 4:

Si has descargado este repositorio completo de GitLab tendrás en su interior los siguientes archivos:
1. create_index_html.sh
2. Dockerfile
3. overlay_network.sh
4. prueba.txt
5. README.md (este archivo)

El archivo más importante de este repositorio es `overlay_network.sh`, este archivo es el script principal para la creación de la red.

Vamos a realizar este ejemplo utilizando dos hosts diferentes conectados a la misma LAN (podrían ser máquinas virtuales dentro de un host si están conectadas a la misma red interna)  en los que lanzaremos unos cuantos contenedores.

Los hosts serán `HOST1` y `HOST2`:
En `HOST1` lanzaremos 3 contenedores (cont1,cont2,cont3) conectados a la red 192.168.15.0/24 mediante el bridge 192.168.15.1.
Estos contenedores tendrán las direcciones 192.168.15.2, 192.168.15.3 y 192.168.15.4 y expondrán mediante reglas de iptables 
el servidor web (funcionando en el puerto 5000 del contenedor) por la dirección IP del `HOST1` en los puertos 5002, 5003 y 5004 respectivamente.

En `HOST2` lanzaremos 2 contenedores (cont4,cont5) conectados a la red 192.168.15.0/24 mediante el bridge 192.168.15.5.
Estos contenedores tendrán las direcciones 192.168.15.6 y 192.168.15.7 y expondrán mediante reglas de iptables 
el servidor web (funcionando en el puerto 5000 del contenedor) por la dirección IP del `HOST2` en los puertos 5006 y 5007 respectivamente.

Tras ejecutar el comando que añade los pasos necesarios para completar el ejercicio 4, los contenedores de ambas redes no sólo podrán ver el servidor web expuesto a través de la IP del `HOSTX`, si no que serán capaces de alcanzar cualquier servicio expuesto por el contenedor a través de la propia dirección IP del contenedor.

## El ejemplo
Primero pegaré los comandos a utilizar y luego explicaré que hace cada uno.

[Dentro del HOST1]
```bash
./overlay_network --setup-download -c cont1,cont2,cont3 -d 192.168.15.1/24 IP_HOST1/MASK_HOST1
./overlay_network -4 2 192.168.15.5/24 IP_HOST2/MASK_HOST2
```

[Dentro del HOST2]
```bash
./overlay_network --setup-download -c cont4,cont5 -d 192.168.15.5/24 IP_HOST2/MASK_HOST1
./overlay_network -4 3 192.168.15.1/24 IP_HOST1/MASK_HOST1
```

Tras ejecutar todos estos comandos podremos:
1. hacer ping entre los contenedores situados en el mismo HOST utilizando el bridge
2. hacer ping de los contenedores al HOST que los contiene y del HOST a los contenedores
3. hacer ping a direcciones alcanzables desde el HOST (como el 8.8.8.8 si tenemos conexión a Internet desde el HOST) utilizando la ruta por defecto
4. navegar a los servidores web situados dentro de los contenedores a través de la IP del HOST que los expone desde cualquier dispositivo conectado a la misma LAN que el HOST

Y por último, si el comando -4 es utilizado:
5. podremos alcanzar desde los contenedores situados dentro del HOST y desde el propio HOST a cualquier servicio en los contenedores del otro HOST

## Las pruebas
Ejemplos:
1. Desde cont1 (192.168.15.2) en `HOST1`: ping 192.168.15.3
2. Desde cont2 (192.168.15.3) en `HOST1`: ping IP_HOST1
   Desde `HOST1`: ping 192.168.15.3
3. Desde cont3 (192.168.15.4) en `HOST1`: ping 8.8.8.8
4. Desde `HOST2`: curl IP_HOST1:5002 (Y así accederíamos al servidor web en el cont1)
5. Desde cont1 (192.168.15.2) en `HOST1`: ping 192.168.15.6
   Desde cont1 (192.168.15.2) en `HOST1`: curl 192.168.15.6:5000

Esto son solo ejemplos, se puede alcanzar todo lo comentado desde cualquier combinación de HOST y contenedor.

## La explicación de los comandos y las opciones
```bash
./overlay_network --setup-download -c cont1,cont2,cont3 -d 192.168.15.1/24 IP_HOST1/MASK_HOST1
```
En este comando empezamos con la opción --setup-download que descargará la imagen desde Docker Hub si no está presente en el listado de imágenes de tu Docker. Si está presente una imagen con el mismo nombre, omitirá la descarga. Debido a un bug que no he podido ojear por falta de tiempo, esta opción es obligatoria, se quiera descargar o no.

Con la opción -c y especificando un string en formato CSV le decimos al script que cree tantos contenedores como nombres separados por comas hayan presentes en esa opción. Esta opción se puede utilizar con un fichero también. El fichero prueba.txt es un ejemplo bastante mal formateado que el programa es capaz de arreglar y parsear (como ejemplo).

Con la opción -d realizamos el "despliegue" esto añade el bridge, las redes internas a los contenedores y los comandos iptables en el HOST correspondiente para exponer los servicios de los contenedores. Esta opción requiere de la dirección IP y la máscara del bridge a utilizar por la red y de la dirección IP y la máscara del HOST donde se está ejecutando todo. Ahora mismo es necesario especificar la red 192.168.15.0/24 como red para los contenedores de cualquier otra forma los parseadores de direcciones IP utilizados internamente por el script se quejarán. Sin implementar estos parseadores, cualquier dirección sería válida, el script está preparado para utilizar cualquier dirección IP.

```bash
./overlay_network -4 2 192.168.15.5/24 IP_HOST2/MASK_HOST2
```

Este comando lo ejecutamos tras haber ejecutado el primero, obligatoriamente. Con la opción -4 indicamos que queremos añadir la funcionalidad del paso 4 del ejercicio especificado. Esto requiere de los siguientes argumentos: 2 es el número de contenedores desplegados en el HOST al que queremos conectar, 192.168.15.5/24 es la dirección IP del bridge de la red de contenedores a la que queremos conectar y por último IP_HOST2/MASK_HOST2 es la dirección del HOST que ejecuta los contenedores a los que queremos conectar.

Por último comentar que el script contiene un pequeño help, se puede ejecutar con:
```bash
./overlay_network -h
```
Donde se muestran más opciones como -t para eliminar todo lo desplegado (excepto el paso 4, por falta de tiempo).

## Comentarios sobre algunos archivos
El archivo `create_index_html.sh` se ejecuta dentro de los contenedores para crear una página HTML que muestre el ID del contenedor donde se está lanzando el servidor web y así poder verificar más fácil que todo funciona correctamente.