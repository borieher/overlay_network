#!/bin/bash

# Bash variable to store working directory
work_dir="/opt/html"

# Create working directory
mkdir "${work_dir}"

# Create an index.html file and populate it
touch "${work_dir}"/index.html
echo "<h1>Welcome to container's "$(hostname)" webpage!</h1>" > "${work_dir}"/index.html