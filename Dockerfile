FROM ubuntu:latest

# Update apt-get repositories before installing packages
RUN apt-get update -y

# Install iproute2 for ip link, ip route... commands
RUN apt-get install -y iproute2

# Install iputils-ping for the ping tool
RUN apt-get install -y iputils-ping

# Install Python 3 and
RUN apt-get install -y python3

# Copy the webpage index.html script
COPY create_index_html.sh /opt

# Declare the port number the container should expose
EXPOSE 5000

# Prepare the webpage the container will share and execute an HTTP server running on container port 5000
CMD ./opt/create_index_html.sh && python3 -m http.server 5000 --directory /opt/html